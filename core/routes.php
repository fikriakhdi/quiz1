<?php
//basic routing, by exploding every part of URL
$link = preg_replace('[^a-z0-9-]','',$_SERVER['REQUEST_URI']);
if ($_SERVER['HTTP_HOST'] == '127.0.0.1' or $_SERVER['HTTP_HOST'] == 'localhost') { $link = substr($link,1,200); };
$url  = (explode('/',$link));
$url_1 	= isset($url[1]) ? $url[1] : '';
$url_2 	= isset($url[2]) ? $url[2] : '';
$url_3  = isset($url[3]) ? $url[3] : '';
$url_4 	= isset($url[4]) ? $url[4] : '';
$url_5 	= isset($url[5]) ? $url[5] : '';
$url_6 	= isset($url[6]) ? $url[6] : '';

if($url_1=="api"){
    if($url_2=="customers"){
        if($url_4=="comments"){
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $customer_id = $url_3;
                include "customers/post_comment.php";
            } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $customer_id = $url_3;
                include "customers/get_comment.php";
            } else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
                $customer_id = $url_3;
                include "customers/like_comment.php";
            }
        }
    } else if($url_2=="admin"){
        if($url_4=="comments"){
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $admin_id = $url_3;
                include "admin/post_comment.php";
            } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $admin_id = $url_3;
                include "admin/get_comment.php";
            } else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
                $admin_id = $url_3;
                include "admin/like_comment.php";
            }
        } 
    }
} else if($url_1==""){
    include "customers/index.php";
} else if($url_1=="admin"){
    if($url_2=="approval"){
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            include "admin/approval.php";
        }
    } else if($url_2=="approve_comment"){
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $comment_id = $url_3;
            include "admin/approve_comment.php";
        }
    } else
    include "admin/index.php";
} else if($url_1=="register"){
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $customer_id = $url_3;
        include "customers/register_customer.php";
    } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $customer_id = $url_3;
        include "customers/register.php";
    } 
} else if($url_1=="register_admin"){
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $adminId = $url_3;
        include "admin/register_admin.php";
    } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        $adminId = $url_3;
        include "admin/register.php";
    } 
}
?>