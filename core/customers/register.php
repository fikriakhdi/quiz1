<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Page</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <style>
    body{
    background: #F8DA56;
}
.form-login{
    margin-top: 13%;
}
.outter-form-login {
    padding: 20px;
    background: #EEEEEE;
    position: relative;
    border-radius: 5px;
}
.logo-login {
    position: absolute;
    font-size: 35px;
    background: #21A957;
    color: #FFFFFF;
    padding: 10px 18px;
    top: -40px;
    border-radius: 50%;
    left: 40%;
}
.inner-login .form-control {
    background: #D3D3D3;
}
h3.title-login {
    font-size: 20px;
    margin-bottom: 20px;
}

.forget {
    margin-top: 20px;
    color: #ADADAD;
}
.btn-custom-green {
    background: #21A957;
    color: #fff;
}
    </style>
  </head>
  <body>
    <div class="col-md-4 col-md-offset-4 form-login">
    
        <div class="outter-form-login">
        <div class="logo-login">
            <em class="glyphicon glyphicon-user"></em>
        </div>
            <form action="register" class="inner-login" method="post">
            <h3 class="text-center title-login">Login Customer</h3>
                <div class="form-group">
                    <input type="text" class="form-control" name="full_name" placeholder="Full Name" required>
                </div>

                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                </div>
                
                <input type="submit" class="btn btn-block btn-custom-green" value="LOGIN" />
                
            </form>
        </div>
    </div>
  </body>
</html>