<?php

include "core/connection.php";
$stm = $pdo->query("SELECT comments.*,user.id userId, user.full_name, user.email, user.role FROM comments JOIN user ON comments.creator =  user.id WHERE comments.approved=0");
$data = $stm->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Approve Comments</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <style>
    body{
    background: #FFFFFF;
}
.form-login{
    margin-top: 13%;
}
.outter-form-login {
    padding: 20px;
    background: #EEEEEE;
    position: relative;
    border-radius: 5px;
}
.logo-login {
    position: absolute;
    font-size: 35px;
    background: #21A957;
    color: #FFFFFF;
    padding: 10px 18px;
    top: -40px;
    border-radius: 50%;
    left: 40%;
}
.inner-login .form-control {
    background: #D3D3D3;
}
h3.title-login {
    font-size: 20px;
    margin-bottom: 20px;
}

.forget {
    margin-top: 20px;
    color: #ADADAD;
}
.btn-custom-green {
    background: #21A957;
    color: #fff;
}
    </style>
  </head>
  <body>
    <div class="col-md-12  form-login">
    <h4 style="text-align:center">Comment List</h4>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>
                Full name
                </th>
                <th>
                Email
                </th>
                <th>
                Content
                </th>
                <th>
                Approve
                </th>
            </tr>
            </thead>
            <tbody>
                <?php if($data){ ?>
                
                <?php 
                foreach($data as $item){ ?>
                <tr>
                    <td>
                    <?php echo $item['full_name'];?>
                    </td>
                    <td>
                    <?php echo $item['email'];?>
                    </td>
                    <td>
                    <?php echo $item['content'];?>
                    </td>
                    <td>
                    <a href="approve_comment/<?php echo $item['id'];?>" class="btn btn-success">Approve</a>
                    </td>
                </tr>
                <?php }?>
                <?php } ?>
            </tbody>
        </table>
    </div>
    </div>
  </body>
</html>