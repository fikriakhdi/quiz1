<?php
if(empty($_COOKIE['admin']))header("location:register_admin");
?>
<!doctype html>
<html>
	<head>
		<meta charset=utf-8>
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<title>Jquery Comments Plugin</title>

		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="css/jquery-comments.css">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

		<!-- Libraries -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery-comments.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
		<style type="text/css">
			body {
				padding: 20px;
				margin: 0px;
				font-size: 14px;
				font-family: "Arial", Georgia, Serif;
			}
		</style>

		<!-- Init jquery-comments -->
		<script type="text/javascript">
			$(function() {
      

      var saveComment = function(data) {
	          // Convert pings to human readable format
	          $(Object.keys(data.pings)).each(function(index, userId) {
	              var fullname = data.pings[userId];
	              var pingText = '@' + fullname;
	              data.content = data.content.replace(new RegExp('@' + userId, 'g'), pingText);
	          });
              
	          return data;
		    }
		    
		    $('#comments-container').comments({
                  currentUserIsAdmin: true,
		          textareaPlaceholderText: 'Yorum Yap',
		          newestText: 'En Yeniler',
		          oldestText: 'Son Eklenen',
		          popularText: 'Popüler',
		          sendText: 'Gönder',
		          replyText: 'Yanıtla',
		          editText: 'Düzenle',
		          editedText: 'Düzenlendi',
		          youText: 'Sen',
		          saveText: 'Kaydet',
		          deleteText: 'Sil',
		          newText: 'Yeni',
		          viewAllRepliesText: '__replyCount__ yanıtları görüntüle',
		          hideRepliesText: 'Yanıtları Gizle',
		          noCommentsText: 'Yorum Yapılmadı',

		          profilePictureURL: 'https://viima-app.s3.amazonaws.com/media/public/defaults/user-icon.png',
		          currentUserId: $.cookie("customer"),
		          roundProfilePictures: false,
		          textareaRows: 1,
		          enableHashtags: true,
		          getComments: function(success, error) {
                      var adminId = $.cookie("admin");
                      console.log(adminId)
                    $.getJSON('api/admin/'+adminId+'/comments', function (data) {
                        var result = jQuery.parseJSON(JSON.stringify(data))
                        if (result.status == 200) {
                            setTimeout(function() {
                                console.log(result)
                            success(result.data);
                            }, 500);
                        } else {
                            alert('failed to load data')
                        }
                    })
		          },
		          postComment: function(data, success, error) {
                      console.log(data)
                      var adminId = $.cookie("admin");
                    $.post('api/admin/'+adminId+'/comments',data,
                    function (ret) {
                        var result = JSON.parse(ret)
                        if (result.status == 200) {
                            setTimeout(function() {
                            success(saveComment(data));
                            }, 500);
                        } else {
                            alert('failed to save comment');
                        }
                        })
		          },
		          // putComment: function(data, success, error) {
		          //   setTimeout(function() {
		          //     success(saveComment(data));
		          //   }, 500);
		          // },
		          // deleteComment: function(data, success, error) {
		          //   setTimeout(function() {
		          //     success();
		          //   }, 500);
		          // },
		          upvoteComment: function(data, success, error) {
                      var adminId = $.cookie("admin");
                    $.ajax({
                        url:'api/admin/'+adminId+'/comments',
                        type:"PUT",
                        data:data,
                        success:function (ret) {
                        var result = JSON.parse(ret)
                        if (result.status == 200) {
                            setTimeout(function() {
                            success(data);
                            }, 500);
                        } else {
                            alert('failed to like comment');
                        }
                        }})
		          },
		          // uploadAttachments: function(dataArray, success, error) {
		          //   setTimeout(function() {
		          //     success(dataArray);
		          //   }, 500);
		          // },
		        });
			});
		</script>

	</head>
	<body>
    <a href="admin/approval">Approval Page</a><br><br>
    <span><?php echo $_COOKIE['admin_full_name'];?> </span><span><?php echo $_COOKIE['admin_email'];?> </span>
		<br> <br> <div id="comments-container"></div>
	</body>
</html>
