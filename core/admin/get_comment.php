<?php
include "core/connection.php";
$stm = $pdo->query("SELECT comments.*,user.id userId, user.full_name, user.email, user.role FROM comments JOIN user ON comments.creator =  user.id");
$data = $stm->fetchAll();
$filtered = array();
foreach($data as $item){
    
    $filtered[] = array(
        'id'=>$item['id'],
        'parent'=>($item['parent']!=0?(int)$item['parent']:null),
        'created'=>date('Y-m-d', strtotime($item['created'])),
        'modified'=>date('Y-m-d', strtotime($item['modified'])),
        'content'=>$item['content'],
        'pings'=>$item['pings'],
        'creator'=>$item['creator'],
        'fullname'=>$item['full_name'],
        'profile_picture_url'=>($item['role']=="admin"?"https://viima-app.s3.amazonaws.com/media/public/defaults/user-icon.png":""),
        'created_by_admin'=>($item['role']=="admin"?true:false),
        'created_by_current_user'=>($item['userId']==$_COOKIE['admin']?true:false),//show comment if it's belong to the current user by checking the cookie
        'upvote_count'=>(int)$item['upvote_count'],
        'user_has_upvoted'=>empty($_COOKIE['vote_'.$item['id']])?false:true, //to show liked comment by checking the cookie
        'is_new'=>(time() - strtotime($item['created']) < 3601?true:false), //tagged as "new" within 1 hour 
    );
}
$ret = array('status'=>200, 'data'=>$filtered);
die(json_encode($ret));
?>